#!/bin/bash -x
set -e
set -a

# read gitlab deployment variables, passed through docker via .env file
source .env

# debug
# export

# install systemd service script
cp -f mimir.service /etc/systemd/system/mimir.service
systemctl daemon-reload

# make persistent across reboots
systemctl enable mimir

# login to registry / image service
docker login -u mimir_mvp -p ${REGISTRY_TOKEN} ${REGISTRY}

# stop services and shut down containers
# docker-compose down --remove-orphans || /bin/true
systemctl stop mimir

# download image
docker pull registry.gitlab.com/wobcom/mimir:latest

# run
# manual: docker-compose up -d 
systemctl start mimir

