# Development

## Manual Installation

1. Get Source:

  `$ git clone https://gitlab.service.wobcom.de/infrastructure/mimir`

2. Install dependencies and build in virtual environment:

  `$ cd mimir && make`

3. Start `redis-server´.

3. Run mimir:

  `$ make debug`



## Docker compose

1. Edit ```.env``` file:
```
BIND_HOST=hostname.com
HTTP_BASIC_AUTH='user:$ap$M68z.Zi.am.a.password.hash.tPLf.sk0'
```

2. ```docker-compose up```

