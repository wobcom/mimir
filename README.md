# mimir

This is the repository of the **mimir** network device web API by [WOBCOM GmbH](https://www.wobcom.de/).

![screenshot](screenshot.png)

You can use it to seamlessly integrate shell interactions with network equipment into your [Operational Support System](https://en.wikipedia.org/wiki/Operations_support_system) or into other Provisioning Systems via HTTP.

It provides an RPC-style endpoint to execute remote commands via SSH and evaluate their return codes, even if the device only supports *interactive* shell sessions (see [SSH RFC 4254 6.5](https://tools.ietf.org/html/rfc4254#page-13) for more information).


## Features

- **minimalistic HTTP API** -> simple to use
- **OpenAPI documentation** of methods
- Support for **~50 types of devices and operating systems** via netmiko library ([list of supported devices](https://github.com/ktbyers/netmiko)). 
- **Parse** the output of the commands to structured data via [TextFSM](https://pynet.twb-tech.com/blog/automation/netmiko-textfsm.html).
- **easily extend** device types with custom logic.
  - **example**: extended support for KeyMile MileGate devices. When using parameter `parse_output` the API returns the commands answer in a structure derived from the shells XML mode (CLI: `mode display xml`) instead of using TextFSM.


## Documentation

**Ressource:** /api/v1/device/{hostname}/execute

**Payload:** application/json 

```
{
  "username": "string",
  "password": "string",
  "secret": "string",
  "device_type": "string",
  "port": int,
  "timeout": int,
  "auth_timeout": int,
  "commands": [
    "string",
    "string",
  ],
  "parse_output": bool,
  "stop_on_first_error": bool
}
```

**Response:**

```
{
  "detail": "string",
  "commands": [
    "string"
  ],
  "outputs": [
    {
      "success": bool,
      "detail": "string",
      "command": "string",
      "output": [
        [
          "string"
        ]
      ],
      "execution_time": "string"
    }
  ],
  "execution_time": "string"
}
```

You can find a complete and interactive documentation and test-suite included with the service at 

  * [https://mimir-server/api/v1/docs](https://mimir/api/v1/docs) for SwaggerUI
  
  * [https://mimir/redoc](https://mimir/redoc) for redoc


## Deployment 

via [DOCKER](DOCKER.md)

## Frameworks and Libraries 

  * [python3](https://www.python.org/) licensed under PSF license.

  * [netmiko](https://github.com/ktbyers/netmiko) licensed under the terms of the MIT license.

  * [fastapi](https://fastapi.tiangolo.com/) licensed under the terms of the MIT license.

  * [redis](https://redis.io/) licensed under the terms of the MIT license.

  * [TextFSM](https://github.com/google/textfsm) licensed under the terms of the Google CLA.

## Changes

see [CHANGES](CHANGES.md)

## License

This code is distributed under the GPLv3 license, see the [LICENSE](LICENSE.md) file.

## Authors

Copyright (c) <2019> Thomas Karmann <thomas.karmann@port-zero.com>
