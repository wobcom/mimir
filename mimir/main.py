#!venv/bin/python

import os
import logging
from distutils.util import strtobool

from fastapi import FastAPI
from starlette.responses import RedirectResponse, JSONResponse
from fastapi.exceptions import RequestValidationError

from mimir.definitions import CONFIG_PATH, ROOT_PATH
from mimir.common.settings import Settings
from mimir.common.util import setup_logging

from mimir.actions.execute import execute
from mimir.actions.ping import ping


# Initial setup
settings = Settings("{}/mimir.conf".format(CONFIG_PATH), "global").as_dict()
debug = bool(strtobool(settings.get("debug", "false")))
setup_logging(debug)

logging.info("Starting mimir worker debug_mode:{}".format(str(debug)))

os.environ["NET_TEXTFSM"] = "{}/../ntc-templates/templates".format(ROOT_PATH)
logging.debug(
    "TextFSM templates imported via NET_NETXTFSM={}".format(os.environ["NET_TEXTFSM"])
)

# API definition
app = FastAPI(
    title="mimir - network device gateway",
    description="Talk to your SSH shells, routers, switches, firewalls via HTTP.",
    version="0.4",
    openapi_url="/api/v1/openapi.json",
    docs_url="/api/v1/docs",
    debug=debug,
)

# Routers
app.include_router(execute)
app.include_router(ping)


@app.get("/", tags=[""], summary="Redirect to API documentation.", deprecated=True)
def index():
    return RedirectResponse(url="/api/v1/docs")


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return JSONResponse(status_code=422, content={"detail": str(exc).splitlines()})
