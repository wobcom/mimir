import logging

from logging.handlers import RotatingFileHandler

from time import time


class Timer(object):
    def tick(self):
        self.start_time = time()

    def tock(self):
        return "{:.1f}s".format(time() - self.start_time)


def get_class_name(device_type):
    class_name = device_type.split("_")
    class_name = [c.capitalize() for c in class_name]
    class_name = "".join(class_name)
    return class_name + "NetworkDevice"


def set_logfile(basename, level=logging.INFO):
    logfile = "/var/log/mimir/{}.log".format(basename)
    formatter = logging.Formatter("%(asctime)s %(message)s")

    mimir = logging.getLogger("")
    mimir.setLevel(level)

    handler = logging.handlers.RotatingFileHandler(
        logfile, mode="a", maxBytes=30000, backupCount=1, encoding="utf-8"
    )
    handler.setFormatter(formatter)
    mimir.addHandler(handler)


def setup_logging(debug):
    if debug is True:
        logging.debug("Configure logging for debugging.")
        level_mimir = logging.DEBUG
        level_other = logging.WARNING
        level_netmiko = logging.INFO
    else:
        logging.info("Configure logging for production.")
        level_mimir = logging.INFO
        level_netmiko = level_other = logging.WARNING

    formatter = logging.Formatter("%(asctime)s %(message)s")

    # webserver
    logfile_uvicorn = "/var/log/mimir/http.log"
    uvicorn = logging.getLogger("uvicorn")
    uvicorn.setLevel(logging.ERROR)

    handler = logging.FileHandler(logfile_uvicorn, mode="w", encoding="utf-8")
    handler.setFormatter(formatter)
    uvicorn.addHandler(handler)

    set_logfile("main", level_mimir)

    # console / stdout
    root = logging.getLogger()
    hdlr = root.handlers[0]
    hdlr.setFormatter(formatter)

    # logging.basicConfig(level=level_mimir, format="%(asctime)s %(message)s")

    logger = logging.getLogger("netmiko")
    logger.setLevel(level_netmiko)

    logger = logging.getLogger("paramiko.transport")
    logger.setLevel(level_other)
