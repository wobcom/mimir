#!venv/bin/python
import logging

from fastapi import APIRouter
from starlette.responses import JSONResponse

from netmiko import NetMikoTimeoutException, NetMikoAuthenticationException
from paramiko.ssh_exception import SSHException

from mimir.common.util import Timer, set_logfile

from mimir.actions.globals import NetmikoParameters
from mimir.actions.helpers import get_driver_by_device_type

from .responses import PingResponse, Codes

router = APIRouter()


@router.post(
    "/api/v1/device/{hostname}/ping",
    summary="Authenticate to a network device and return shell prompt",
    tags=["Actions"],
    response_class=JSONResponse,
    response_model=PingResponse,
    response_description="Ping successful",
    responses=Codes,
)
def ping(*, hostname: str, params: NetmikoParameters):
    set_logfile(hostname)
    timer = Timer()
    timer.tick()

    # pass through connection parameters to netmiko handler
    connection_settings = {
        "host": hostname,
        "device_type": params.device_type,
        "port": params.port,
        "username": params.username,
        "password": params.password,
        "timeout": params.timeout,
        "auth_timeout": params.auth_timeout,
        "global_delay_factor": params.delay_factor,
    }

    driver = get_driver_by_device_type(params.device_type)
    args = (connection_settings,)
    kw = {}

    try:
        device = driver(*args, **kw)
        prompt = device.connect()
        success = True
    except (
        NetMikoTimeoutException,
        NetMikoAuthenticationException,
        SSHException,
        OSError,
        ValueError,
    ) as e:
        success = False
        prompt = str(e).replace("\n", " ")

    logging.debug(
        "Ping. host:{} success:{} prompt:'{}'".format(hostname, success, prompt)
    )
    # device.disconnect()

    return JSONResponse(
        status_code=200 if success else 201,
        content={
            "detail": "Ping successful" if success else "Ping failed",
            "prompt": prompt if success else "n/a",
            "execution_time": timer.tock(),
        },
    )
