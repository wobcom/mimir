PYTHON=python3.7
PIP=pip3
PIPFLAGS=$$([ -z "$$VIRTUAL_ENV" ] && echo --user) -U
TEXTFSM_TEMPLATES=https://github.com/krmnn/ntc-templates 

.PHONY: venv test lint deps doc clean help debug run docker textfsm

all: venv

venv: textfsm venv/bin/activate

venv-dev: venv/bin/activate

venv/bin/activate: requirements-dev.txt
	test -d venv || virtualenv --python=python3.7 venv
	. venv/bin/activate; $(PIP) install $(PIPFLAGS) -Ur requirements-dev.txt
	touch venv/bin/activate

deps: venv

clean:
	rm -rf doc/build
	rm -rf venv

textfsm:
	@echo "Updating TextFSM templates..."
	@#git clone https://github.com/networktocode/ntc-templates?__s=XXXXXXXX textfsm
	[ -d ntc-templates ] && cd ntc-templates && git pull || git clone ${TEXTFSM_TEMPLATES} ntc-templates || exit 0 
	export NET_TEXTFSM="${CURDIR}/ntc-templates/templates/"

debug: venv
	venv/bin/uvicorn mimir.main:app --reload

run: venv
	venv/bin/uvicorn mimir.main:app

docker:
	export BIND_HOST=127.0.0.1; docker-compose down --remove-orphans -v --rmi all
	export TEXTFSM_TEMPLATES="https://github.com/krmnn/ntc-templates.git" NET_TEXTFSM="${CURDIR}/ntc-templates/templates/" BIND_HOST=127.0.0.1; docker-compose up --build --force-recreate

lint:
	black mimir
	pylama mimir
